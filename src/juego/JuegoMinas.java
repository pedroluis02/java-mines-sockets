/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.util.Random;

/**
 *
 * @author pedroluis
 */
public class JuegoMinas {
    public static final int NADA = -1;
    public static final int MINA = -2;
    //
    public static final int LIBRE = 0;
    public static final int OCUPADA = 3;
    
    public static final int MARCAR = 0;
    public static final int DESMARCAR = 1;
    
    public static final int EXPLOSION = 6;
    public static final int FIN = 7;
    //
    public static float PORCENTAJE = 6.4F;
    //
    
    public int idJuego;
    public String jugador_1;
    public String jugador_2;
    //
    public int [][]MJ;
    private int numeroFilas, numeroColumnas;
    private int numeroMinas;
    //
    public String MJ_Str="";

    public JuegoMinas(int idJuego, String jugador_1, String jugador_2) {
        this.idJuego = idJuego;
        this.jugador_1 = jugador_1;
        this.jugador_2 = jugador_2;
        
        numeroFilas = 6; numeroColumnas = 6; 
        numeroMinas = Math.round((float)(numeroFilas * numeroColumnas) / PORCENTAJE);
        MJ = new int[numeroFilas][numeroColumnas];
        iniciarMatriz(NADA);
        colocarMinas();
        colocarNumeros();
        convertir_a_Cadena();
    }
    private void iniciarMatriz(int valor) {
        for(int i=0; i<numeroFilas; i++) {
            for(int j=0; j<numeroColumnas; j++) {
                MJ[i][j] = valor;
            }
        }
    }
    private void colocarMinas() {
        Random r = new Random();
        int f, c, i=0;
        while(i < numeroMinas) {
            f = r.nextInt(numeroFilas);
            c = r.nextInt(numeroColumnas);
            if(MJ[f][c] == NADA) {
                MJ[f][c] = MINA;
                i++;
            }
        }
    }
    private int contarMinas_Alrededor(int i, int j) {
        int numero = 0;
        for(int k = (i-1); k <= (i+1);  k++) {
            for (int m = (j-1); m <= (j+1); m++) {
                if(k>=0 && k<numeroFilas && m>=0 && m<numeroColumnas) {
                    if(MJ[k][m] == MINA) {
                        numero++;
                    }
                }
            }
        }
        return numero;
    }
    private void colocarNumeros() {
        for(int i=0; i<numeroFilas; i++) {
            for(int j=0; j<numeroColumnas; j++) {
                if(MJ[i][j] == NADA) {
                    MJ[i][j] = contarMinas_Alrededor(i, j);
                }
            }
        }
    }
    private void convertir_a_Cadena() {
        for(int i=0; i<numeroFilas; i++) {
            for(int j=0; j<numeroColumnas; j++) {
                MJ_Str += MJ[i][j] + ",";
            }
        }
    }
    
}
