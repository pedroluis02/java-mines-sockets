/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Random;
import juego.JuegoMinas;

/**
 *
 * @author pedroluis
 */
public class TestMInas {
    public static void imprimir(int M[][], int f, int c) {
        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(M[i][j]+ " ");
            }
            System.out.println();
        }
    }
    public static void asignar(int M[][], int f, int c, int valor) {
        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                M[i][j] = valor;
            }
        }
    }
    public  static int numeroMinas(int M[][], int f, int c, int i, int j) {
        int numero = 0;
        for(int k = (i-1); k <= (i+1);  k++) {
            for (int m = (j-1); m <= (j+1); m++) {
                if(k>=0 && k<f && m>=0 && m<c) {
                    if(M[k][m] == -2) {
                        numero++;
                    }
                }
            }
        }
        return numero;
    }
    public static void numeros(int M[][], int f, int c) {
        uno: for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                if(M[i][j] == -1) {
                    M[i][j] = numeroMinas(M, f, c, i, j);
                }
            }
        }
    }
    public static void minas(int M[][], int f, int c, int numero) {
        int i = 0;
        Random r = new Random();
        while(i < numero){
            int f0 = r.nextInt(f);
            int c0 = r.nextInt(c);
            if(M[f0][c0] == -1) {
                M[f0][c0] = -2;
                i++;
            }            
        }
    }
    public static void main(String[] args) {
        //System.out.println(Math.floor(Math.random()*((i + 1) - (i - 1) + 1) + (i - 1)));
        JuegoMinas j = new JuegoMinas(23, null, null);
        System.out.println(j.MJ_Str);
    }
}
