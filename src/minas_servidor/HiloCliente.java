/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minas_servidor;

/**
 *
 * @author pedroluis
 */

import juego.JuegoMinas;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import mensajes.Mensaje;

public class HiloCliente extends Thread {
    public Socket socket;
    private DataInputStream flujoLectura;
    private DataOutputStream flujoEscritura;
    public int idJuego;
    public int numero;
    public String id;
    public String nombre;
    public String idOponente;
    
    private ServidorGUI servidorGui;

    public HiloCliente(Socket socket, int numero,ServidorGUI servidorGUI) {
        this.socket = socket;
        this.idJuego = -1;
        this.servidorGui = servidorGUI;
        this.numero = -1;
        try {
            this.flujoLectura = new DataInputStream(socket.getInputStream());
            this.flujoEscritura = new DataOutputStream(socket.getOutputStream());
        } catch(IOException ex) {
            servidorGUI.mostrarMensaje(ex.getMessage());
        }
    }
    @Override
    public void run() {
        int opcion;
       try {
           while(true) {   
               opcion = flujoLectura.readInt();
               switch(opcion) {
                   case Mensaje.DATOS:
                       nombre = flujoLectura.readUTF();
                       //registro completo
                       ServidorGUI.jugadores.put(id, this);
                       ServidorGUI.oponentesStr += nombre + "~" + id + ",";
                       //
                       servidorGui.mostrarMensaje("Datos: id=" + id + " nombre=" + nombre);
                       enviarOponentes(ServidorGUI.oponentesStr);
                       servidorGui.enviarJugadorConectado(id, nombre);
                       break;
                   case Mensaje.SOLICITUD:
                       String para = flujoLectura.readUTF();
                       if(ServidorGUI.jugadores.get(para).idJuego == -1) {
                           ServidorGUI.jugadores.get(para).enviarSolicitudJuego(id);
                       } else {
                          ServidorGUI.jugadores.get(id).enviarRptaSolicitud(para, Mensaje.OCUPADO);
                       }
                       break;
                   case Mensaje.RPTA_SOLICITUD:
                        String _para = flujoLectura.readUTF();
                        int res = flujoLectura.readInt();
                        //
                        if(res == Mensaje.SI) {
                            ServidorGUI.jugadores.get(_para).enviarRptaSolicitud(id, Mensaje.SI);
                            if(idJuego == -1 && ServidorGUI.jugadores.get(_para).idJuego == -1) {
                                int sj = ServidorGUI.juegos.size();
                                idJuego = sj;
                                ServidorGUI.juegos.add(new JuegoMinas(sj, _para, id));
                                ServidorGUI.jugadores.get(_para).idJuego = sj;
                                
                                ServidorGUI.jugadores.get(_para).enviarInicioJuego(id, 1, true);
                                ServidorGUI.jugadores.get(id).enviarInicioJuego(_para, 2, false);
                            } else {  
                                ServidorGUI.jugadores.get(_para).enviarUnirse(id);
                                ServidorGUI.jugadores.get(id).enviarUnirse(_para);
                            }
                            
                        } else {
                            ServidorGUI.jugadores.get(_para).enviarRptaSolicitud(id, Mensaje.NO);
                        }
                        break;
                   case Mensaje.JUGADA:
                       //
                       int f = flujoLectura.readInt(),
                           c = flujoLectura.readInt();
                       
                       servidorGui.mostrarMensaje("Jugada: de="+id+ " para="+idOponente+" C("+f+","+c+")");
                       ServidorGUI.jugadores.get(idOponente).enviarJugada(f, c, numero);
                       break;
                       
                  case Mensaje.BANDERA:
                       //
                       int f1 = flujoLectura.readInt(),
                           c1 = flujoLectura.readInt(),
                           tipo = flujoLectura.readInt();
                       
                       servidorGui.mostrarMensaje("Bandera: de=" + id + " para=" + idOponente +
                               " C(" + f1 + "," + c1 + ") tipo=" + tipo);
                       ServidorGUI.jugadores.get(idOponente).enviarMarcarBandera(f1, c1, numero, tipo);
               }
           }
       } catch(Exception ex) {
           servidorGui.mostrarMensaje(ex.getMessage());
       }       
       
       servidorGui.mostrarMensaje("Id[ " + id + "]: desconectado");
       servidorGui.enviarJugadorDesconectado(id, nombre);
    }
    public void enviarRegistroId(String id) {
        this.id = id;
        try {
            flujoEscritura.writeInt(Mensaje.REGISTRO);
            flujoEscritura.writeUTF(id);
            servidorGui.mostrarMensaje("Registro: id=" + id);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarOponentes(String opStr) {
        try {
            flujoEscritura.writeInt(Mensaje.OPONENTES);
            flujoEscritura.writeUTF(opStr);
            servidorGui.mostrarMensaje("Oponentes: para=" + id);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarSolicitudJuego(String idOponente) {
        try {            
            flujoEscritura.writeInt(Mensaje.SOLICITUD);
            flujoEscritura.writeUTF(idOponente);
            flujoEscritura.writeUTF(ServidorGUI.jugadores.get(idOponente).nombre);
            servidorGui.mostrarMensaje("Solicitud: de="  + idOponente + " para=" + id);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarRptaSolicitud(String idOponente, int rpta) {
        try {            
            flujoEscritura.writeInt(Mensaje.RPTA_SOLICITUD);
            flujoEscritura.writeUTF(idOponente);
            flujoEscritura.writeUTF(ServidorGUI.jugadores.get(idOponente).nombre);
            flujoEscritura.writeInt(rpta);
            servidorGui.mostrarMensaje("Solicitud a "+ idOponente);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarUnirse(String idOponente) {
        try {            
            flujoEscritura.writeInt(Mensaje.UNIRSE);
            flujoEscritura.writeUTF("Juego fallido: de=" + id + " con=" + idOponente);
            servidorGui.mostrarMensaje("Unirse fallido: de=" + id + " con=" + idOponente);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarInicioJuego(String idOponente, int numero, boolean turno) {
        try {
            this.idOponente = idOponente;
            this.numero = numero;
            //
            flujoEscritura.writeInt(Mensaje.INICIO);
            flujoEscritura.writeInt(idJuego);
            flujoEscritura.writeInt(numero);
            flujoEscritura.writeUTF(idOponente);
            flujoEscritura.writeUTF(ServidorGUI.jugadores.get(idOponente).nombre);
            flujoEscritura.writeBoolean(turno);
            // matriz juego;
            flujoEscritura.writeUTF(ServidorGUI.juegos.get(idJuego).MJ_Str);
            servidorGui.mostrarMensaje("Juego["+idJuego+"]: jugador="+id+" oponente="+idOponente);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarJugada(int f, int c, int numero) {
        try {
            flujoEscritura.writeInt(Mensaje.JUGADA);
            
            flujoEscritura.writeInt(f);
            flujoEscritura.writeInt(c);
            flujoEscritura.writeInt(numero);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarMarcarBandera(int f, int c, int numero, int op) {
        try {
            flujoEscritura.writeInt(Mensaje.BANDERA);
            
            flujoEscritura.writeInt(f);
            flujoEscritura.writeInt(c);
            flujoEscritura.writeInt(op);
            flujoEscritura.writeInt(numero);
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarConectado(String id, String nombre) {
        try {
            flujoEscritura.writeInt(Mensaje.NUE_JUGADOR);
            flujoEscritura.writeUTF(id);
            flujoEscritura.writeUTF(nombre);
            servidorGui.mostrarMensaje("Id[" + this.id + "]: envio [" + id +"] conectado");
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
    public void enviarDesconectado(String id, String nombre) {
        if(id.compareTo(idOponente) == 0) {
            idJuego = -1;
        }
        try {
            flujoEscritura.writeInt(Mensaje.DES_JUGADOR);
            flujoEscritura.writeUTF(id);
            flujoEscritura.writeUTF(nombre);
            servidorGui.mostrarMensaje("Id[" + this.id + "]: envio [" + id +"] desconectado");
        } catch(Exception ex) {
            servidorGui.mostrarMensaje(ex.getMessage());
        }
    }
}