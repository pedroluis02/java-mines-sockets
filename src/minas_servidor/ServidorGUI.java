/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minas_servidor;

import juego.JuegoMinas;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author pedroluis
 */
public class ServidorGUI extends javax.swing.JFrame {

    /**
     * Creates new form ServidorGUI
     */
    public ServidorGUI() {
        initComponents();
        botonIniciar.setVisible(false); 
        botonParar.setVisible(false);
        jugadores = new HashMap<>();
        mensajes = new ArrayList<>();
        juegos = new ArrayList<>();
        oponentesStr = "";
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch(Exception ex) {
            mostrarMensaje(ex.getMessage());
            ip = "localhost";
        }
        textoDirIP.setText(ip);
        textoPuerto.setText(PUERTO + "");
    }
    public void iniciarServidor() {
        try {
            serverSocket = new ServerSocket(PUERTO);
            areaTextoConsola.setText(new Date().toString()+": servidor iniciado");
            while(true) {
                Socket socket = serverSocket.accept();
                HiloCliente hj = new HiloCliente(socket, jugadores.size(), this);
                hj.start();
                
                String id = new Date().getTime() + "";
                
                hj.enviarRegistroId(id);
            }
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void mostrarMensaje(String mensaje) {
        areaTextoConsola.append("\n" + mensaje);
        System.out.println(mensaje);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        textoDirIP = new javax.swing.JTextField();
        textoPuerto = new javax.swing.JTextField();
        botonIniciar = new javax.swing.JButton();
        botonParar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        areaTextoConsola = new javax.swing.JTextArea();
        jToolBar1 = new javax.swing.JToolBar();
        botonSalir = new javax.swing.JButton();
        botonLimpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.X_AXIS));

        textoDirIP.setEditable(false);
        textoDirIP.setToolTipText("Dirección del Servidor");
        jPanel1.add(textoDirIP);

        textoPuerto.setEditable(false);
        textoPuerto.setToolTipText("Puerto de Escucha");
        jPanel1.add(textoPuerto);

        botonIniciar.setText("Iniciar");
        botonIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIniciarActionPerformed(evt);
            }
        });
        jPanel1.add(botonIniciar);

        botonParar.setText("Parar");
        botonParar.setEnabled(false);
        jPanel1.add(botonParar);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Consola"));
        jPanel2.setLayout(new java.awt.BorderLayout());

        areaTextoConsola.setEditable(false);
        areaTextoConsola.setColumns(20);
        areaTextoConsola.setRows(5);
        jScrollPane2.setViewportView(areaTextoConsola);

        jPanel2.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        botonSalir.setText("Salir");
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });

        botonLimpiar.setText("Limpiar");
        botonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 506, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonSalir)
                    .addComponent(botonLimpiar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIniciarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botonIniciarActionPerformed

    private void botonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiarActionPerformed
        // TODO add your handling code here:
        areaTextoConsola.setText("");
        areaTextoConsola.setText(new Date().toGMTString()+": servidor iniciado");
    }//GEN-LAST:event_botonLimpiarActionPerformed

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        // TODO add your handling code here:
        cerrarConexiones();
        System.exit(0);
    }//GEN-LAST:event_botonSalirActionPerformed

    private void cerrarConexiones() {
        for(String k : jugadores.keySet()) {
            //jugadores.get(k).stop();
            jugadores.get(k).interrupt();
            try {
                jugadores.get(k).socket.close();
            } catch(Exception ex) {
                mostrarMensaje(ex.getMessage());
            }
        }
        jugadores.clear();
    }
    
    public void enviarJugadorConectado(String id, String nombre) {
        for(String k : jugadores.keySet()) {
            if(jugadores.get(k).id.compareTo(id) != 0) {
               jugadores.get(k).enviarConectado(id, nombre);
            }
        }
    }
    
    public void enviarJugadorDesconectado(String id, String nombre) {
        jugadores.remove(id);
        oponentesStr = "";
        for(String k : jugadores.keySet()) {
            jugadores.get(k).enviarDesconectado(id, nombre);
            oponentesStr += jugadores.get(k).nombre + "~"+ k  + ",";
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaTextoConsola;
    private javax.swing.JButton botonIniciar;
    private javax.swing.JButton botonLimpiar;
    private javax.swing.JButton botonParar;
    private javax.swing.JButton botonSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField textoDirIP;
    private javax.swing.JTextField textoPuerto;
    // End of variables declaration//GEN-END:variables
    
    private ServerSocket serverSocket;
    public static HashMap <String, HiloCliente> jugadores;
    public static ArrayList <String> mensajes;
    public static ArrayList <JuegoMinas> juegos;
    public static String oponentesStr;
    private int PUERTO = 2013;
}
