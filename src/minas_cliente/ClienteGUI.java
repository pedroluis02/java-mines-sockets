
package minas_cliente;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.Timer;
import juego.JuegoMinas;

/**
 * @author pedroluis
*/

public class ClienteGUI extends javax.swing.JFrame implements ActionListener,
        MouseListener {
    public ClienteGUI() {
        initComponents();
        modeloLista = new DefaultListModel<>();
        listOponentes.setModel(modeloLista);
        tiempoMensaje = new Timer(5000, this);
        //
        tiempoMensaje.setActionCommand("tiempo");
        
        String ip, h;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
            h = InetAddress.getLocalHost().getHostName();
        } catch(Exception ex) {
            ip = "localhost";
            h  = "";  
            mostrarMensajeBarra(ex.getMessage());
        }
        textoNombre.setText(h);
        textoDirIP.setText(ip);
        textoPuerto.setText(PUERTO + "");
        //
        numeroFilas = 6; numeroColumnas = 6;
        MTB = new JToggleButton[numeroFilas][numeroColumnas];
        iniciarDatosJuego();
        //
        cantidadLibres = numeroFilas * numeroColumnas;
        numeroMinas = Math.round(
                (float)(numeroFilas * numeroColumnas) / JuegoMinas.PORCENTAJE);
        iniciado = false;
        iniciarIconosNumeros();
    }
    private void iniciarDatosJuego() {
        Component cc[] = panelJuego.getComponents();
        int k = 0;
        for(int i=0; i < numeroFilas; i++) {
            for(int j=0; j < numeroColumnas; j++) {
                JToggleButton t = (JToggleButton)cc[k];
                t.setActionCommand(i+"-"+j);
                
                t.setEnabled(false); 
                Font f = t.getFont();
                t.setFont(new Font(f.getFontName(), Font.BOLD, 25));
                t.setForeground(Color.BLUE);
                t.setDisabledIcon(new 
                        ImageIcon(getClass().getResource("/recursos/nada.png")));
                t.addActionListener(this);
                t.addMouseListener(this);
                MTB[i][j] = t; 
                k++;
            }
        }
    }
    private String conectar(String host, int puerto) {
        try {
            socket = new Socket(host, puerto);
            hcm = new HiloConexion(socket, textoNombre.getText(), this);
            hcm.start();
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    } 
    private String desconectar() {
        try {
            hcm.stop();
            socket.close();
            
            cambioGuiDesconectado();
            
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    //
    public void cambioGuiConectado() {
        botonConectar.setEnabled(false);
        botonDesconectar.setEnabled(true);
        //
        textoNombre.setEditable(false);
        textoDirIP.setEditable(false);
        textoPuerto.setEditable(false);
        //
    }
    public void cambioGuiDesconectado() {
        botonConectar.setEnabled(true);
        botonDesconectar.setEnabled(false);
        //
        modeloLista.clear();
        //
        textoNombre.setEditable(true);
        textoDirIP.setEditable(true);
        textoPuerto.setEditable(true);
        //
        textoTu.setText("");
        textoNombreOP.setText("");
        textoOponente.setText("");
        //
        iniciado = false;
        //
        mostrarMensajeBarra("Te has desconectado");
        hBotonesJuego(false);
        setTitle("Juego Cliente");
    }
    //

    private void iniciarIconosNumeros() {
        iconosNumeros = new ImageIcon[10];
        
        iconosNumeros[0] = recursoIconoNumero("cero");
        iconosNumeros[1] = recursoIconoNumero("uno");
        iconosNumeros[2] = recursoIconoNumero("dos");
        iconosNumeros[3] = recursoIconoNumero("tres");
        iconosNumeros[4] = recursoIconoNumero("cuatro");
        iconosNumeros[5] = recursoIconoNumero("cinco");
        iconosNumeros[6] = recursoIconoNumero("seis");
        iconosNumeros[7] = recursoIconoNumero("siete");
        iconosNumeros[8] = recursoIconoNumero("ocho");
        iconosNumeros[9] = recursoIconoNumero("nueve");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelJuego = new javax.swing.JPanel();
        cuadroTB_00 = new javax.swing.JToggleButton();
        cuadroTB_01 = new javax.swing.JToggleButton();
        cuadroTB_02 = new javax.swing.JToggleButton();
        cuadroTB_03 = new javax.swing.JToggleButton();
        cuadroTB_04 = new javax.swing.JToggleButton();
        jToggleButton6 = new javax.swing.JToggleButton();
        cuadroTB_10 = new javax.swing.JToggleButton();
        cuadroTB_11 = new javax.swing.JToggleButton();
        cuadroTB_12 = new javax.swing.JToggleButton();
        cuadroTB_13 = new javax.swing.JToggleButton();
        cuadroTB_14 = new javax.swing.JToggleButton();
        jToggleButton7 = new javax.swing.JToggleButton();
        cuadroTB_20 = new javax.swing.JToggleButton();
        cudroTB_21 = new javax.swing.JToggleButton();
        cudroTB_22 = new javax.swing.JToggleButton();
        cudroTB_23 = new javax.swing.JToggleButton();
        cudroTB_24 = new javax.swing.JToggleButton();
        jToggleButton8 = new javax.swing.JToggleButton();
        cudroTB_30 = new javax.swing.JToggleButton();
        cudroTB_31 = new javax.swing.JToggleButton();
        cudroTB_32 = new javax.swing.JToggleButton();
        cudroTB_33 = new javax.swing.JToggleButton();
        cudroTB_34 = new javax.swing.JToggleButton();
        jToggleButton9 = new javax.swing.JToggleButton();
        cudroTB_40 = new javax.swing.JToggleButton();
        cudroTB_41 = new javax.swing.JToggleButton();
        cudroTB_42 = new javax.swing.JToggleButton();
        cudroTB_43 = new javax.swing.JToggleButton();
        cudroTB_44 = new javax.swing.JToggleButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        jToggleButton3 = new javax.swing.JToggleButton();
        jToggleButton4 = new javax.swing.JToggleButton();
        jToggleButton5 = new javax.swing.JToggleButton();
        jToggleButton10 = new javax.swing.JToggleButton();
        jToggleButton11 = new javax.swing.JToggleButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        botonAbandonar = new javax.swing.JButton();
        textoTu = new javax.swing.JTextField();
        textoOponente = new javax.swing.JTextField();
        textoNombreOP = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        textoNombre = new javax.swing.JTextField();
        textoDirIP = new javax.swing.JTextField();
        textoPuerto = new javax.swing.JTextField();
        botonConectar = new javax.swing.JButton();
        botonDesconectar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jToolBar2 = new javax.swing.JToolBar();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        textoMensaje = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listOponentes = new javax.swing.JList();
        botonSolicitud = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Juego Cliente");

        panelJuego.setBorder(javax.swing.BorderFactory.createTitledBorder("Juego"));
        panelJuego.setLayout(new java.awt.GridLayout(6, 6, 10, 10));
        panelJuego.add(cuadroTB_00);
        panelJuego.add(cuadroTB_01);
        panelJuego.add(cuadroTB_02);
        panelJuego.add(cuadroTB_03);
        panelJuego.add(cuadroTB_04);
        panelJuego.add(jToggleButton6);
        panelJuego.add(cuadroTB_10);
        panelJuego.add(cuadroTB_11);
        panelJuego.add(cuadroTB_12);
        panelJuego.add(cuadroTB_13);
        panelJuego.add(cuadroTB_14);
        panelJuego.add(jToggleButton7);
        panelJuego.add(cuadroTB_20);
        panelJuego.add(cudroTB_21);
        panelJuego.add(cudroTB_22);
        panelJuego.add(cudroTB_23);
        panelJuego.add(cudroTB_24);
        panelJuego.add(jToggleButton8);
        panelJuego.add(cudroTB_30);
        panelJuego.add(cudroTB_31);
        panelJuego.add(cudroTB_32);
        panelJuego.add(cudroTB_33);
        panelJuego.add(cudroTB_34);
        panelJuego.add(jToggleButton9);

        cudroTB_40.setName(""); // NOI18N
        panelJuego.add(cudroTB_40);
        panelJuego.add(cudroTB_41);
        panelJuego.add(cudroTB_42);
        panelJuego.add(cudroTB_43);
        panelJuego.add(cudroTB_44);
        panelJuego.add(jToggleButton1);
        panelJuego.add(jToggleButton2);
        panelJuego.add(jToggleButton3);
        panelJuego.add(jToggleButton4);
        panelJuego.add(jToggleButton5);
        panelJuego.add(jToggleButton10);
        panelJuego.add(jToggleButton11);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de Juego"));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel1.setText("Tú: ");

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel2.setForeground(java.awt.Color.blue);
        jLabel2.setText("Op:");

        botonAbandonar.setText("Abandonar");
        botonAbandonar.setEnabled(false);

        textoTu.setEditable(false);
        textoTu.setColumns(10);

        textoOponente.setEditable(false);
        textoOponente.setColumns(10);

        textoNombreOP.setEditable(false);
        textoNombreOP.setColumns(5);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonAbandonar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textoNombreOP)
                    .addComponent(textoTu, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                    .addComponent(textoOponente))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(textoTu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(textoNombreOP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textoOponente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonAbandonar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuración"));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.LINE_AXIS));

        textoNombre.setToolTipText("Nombre de Jugador");
        jPanel1.add(textoNombre);

        textoDirIP.setToolTipText("Dirección IP del Servidor");
        jPanel1.add(textoDirIP);

        textoPuerto.setToolTipText("Puerto de conexion");
        jPanel1.add(textoPuerto);

        botonConectar.setText("Conectar");
        botonConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConectarActionPerformed(evt);
            }
        });
        jPanel1.add(botonConectar);

        botonDesconectar.setText("Desconectar");
        botonDesconectar.setEnabled(false);
        botonDesconectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDesconectarActionPerformed(evt);
            }
        });
        jPanel1.add(botonDesconectar);
        jPanel1.add(jSeparator2);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);
        jToolBar2.add(jSeparator1);

        textoMensaje.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jToolBar2.add(textoMensaje);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Oponentes"));
        jPanel3.setLayout(new java.awt.BorderLayout());

        listOponentes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listOponentes.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listOponentesValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(listOponentes);

        jPanel3.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        botonSolicitud.setText("Enviar socitud de juego");
        botonSolicitud.setEnabled(false);
        botonSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSolicitudActionPerformed(evt);
            }
        });
        jPanel3.add(botonSolicitud, java.awt.BorderLayout.PAGE_END);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelJuego, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE))
                    .addComponent(panelJuego, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(12, 12, 12)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConectarActionPerformed
        // TODO add your handling code here:
        String co = conectar(textoDirIP.getText(), PUERTO);
            if(!co.isEmpty()) {
                mostrarMensajeBarra(co);
            }
    }//GEN-LAST:event_botonConectarActionPerformed

    private void botonDesconectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDesconectarActionPerformed
        // TODO add your handling code here:
        String m = desconectar();
        if(!m.isEmpty()) {
            mostrarMensajeBarra(m);
        }
    }//GEN-LAST:event_botonDesconectarActionPerformed

    private void listOponentesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listOponentesValueChanged
        // TODO add your handling code here:
        int i = listOponentes.getSelectedIndex();
        if(i != -1 && !botonAbandonar.isEnabled()) {
            botonSolicitud.setEnabled(true);
        } else {
            botonSolicitud.setEnabled(false);
        }
    }//GEN-LAST:event_listOponentesValueChanged

    private void botonSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSolicitudActionPerformed
        // TODO add your handling code here:
        String []idO = listOponentes.getSelectedValue().toString().split("~");
        String c = hcm.enviarSolicitud(idO[1]);
        if(c.isEmpty()) {
            mostrarMensajeBarra("Solicitud de juego a: " + idO[0] + " - " + idO[1]);
        } else {
            mostrarMensajeBarra(c);
        }
    }//GEN-LAST:event_botonSolicitudActionPerformed
    public void hBotonesJuego(boolean t) {
        for(int i=0; i<numeroFilas; i++) {
            for(int j=0; j<numeroColumnas; j++) {
                MTB[i][j].setEnabled(t);
                MTB[i][j].setIcon(null);
            }
        } 
    }
    private void terminarJuego(int numero, int op) {
        int contador1 = 0;
        int contador2 = 0;
        for (int i = 0; i < numeroFilas; i++) {
            for (int j = 0; j < numeroColumnas; j++) {
                if(MP[i][j] == JuegoMinas.LIBRE) {
                    if(MJ[i][j] == JuegoMinas.MINA) {
                        MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/mina.png")));
                    }
                } else if(MP[i][j] != JuegoMinas.OCUPADA) {
                    if(numero == hcm.numero) {
                        if(MP[i][j] == numero && MJ[i][j] == JuegoMinas.MINA) {
                           MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/banderaok.png"))); 
                            contador1++;
                        } else if(MP[i][j] == numero && MJ[i][j] != JuegoMinas.MINA) {
                            MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/banderaerror.png"))); 
                        } else if(MP[i][j] != numero && MJ[i][j] == JuegoMinas.MINA) {
                           MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/bandera2ok.png"))); 
                            contador2++;
                        } else if(MP[i][j] != numero && MJ[i][j] != JuegoMinas.MINA) {
                            MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/bandera2error.png"))); 
                        }
                    } else {
                        if(MP[i][j] != numero && MJ[i][j] == JuegoMinas.MINA) {
                           MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/banderaok.png"))); 
                            contador2++;
                        } else if(MP[i][j] != numero && MJ[i][j] != JuegoMinas.MINA) {
                            MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/banderaerror.png"))); 
                        } else if(MP[i][j] == numero && MJ[i][j] == JuegoMinas.MINA) {
                           MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/bandera2ok.png"))); 
                            contador1++;
                        } else if(MP[i][j] == numero && MJ[i][j] != JuegoMinas.MINA) {
                            MTB[i][j].setIcon(
                                new ImageIcon(getClass().getResource("/recursos/bandera2error.png"))); 
                        }
                    }
                }
            }
        }
        if(op == JuegoMinas.EXPLOSION) {
            if(numero == hcm.numero) {
                mostrarMensaje("Has perdido: marcados=" + contador1
                        + "\noponente=" + contador2 + "\n Explosion de Mina");
            } else {
                mostrarMensaje("Has ganado: marcados=" + contador2 
                            + "\noponente=" + contador1);
            }
        } else {
            if(contador1 == contador2) {
                mostrarMensaje("Empate: marcados=" + contador1
                        + "\noponente=" + contador2);
            } else if(contador1 > contador2) {
                if(hcm.numero == numero) {
                    mostrarMensaje("Has ganado: marcados=" + contador1
                        + "\noponente=" + contador2);
                } else {
                    mostrarMensaje("Has perdido: marcados=" + contador2
                        + "\noponente=" + contador1);
                }
            } else if(contador1 < contador2) {
                if(hcm.numero == numero) {
                    mostrarMensaje("Has Perdido: marcados=" + contador1
                        + "\noponente=" + contador2);
                } else {
                    mostrarMensaje("Has Ganado: marcados=" + contador2
                        + "\noponente=" + contador1);
                }
            }
        }
        //
        cantidadLibres = numeroFilas * numeroColumnas;
    }
    public void marcar(int f, int c, int numero) {
        if(MJ[f][c] == JuegoMinas.MINA) {
            MTB[f][c].setIcon(new ImageIcon(getClass().getResource("/recursos/explosion.png")));
            // fin juego ----
            MP[f][c] = JuegoMinas.OCUPADA;
            
            terminarJuego(numero, JuegoMinas.EXPLOSION);
            
        } else {
            MTB[f][c].setIcon(iconosNumeros[ MJ[f][c] ]);
            MP[f][c] = JuegoMinas.OCUPADA;
        }
        
        cantidadLibres--;
        
        if(cantidadLibres == 0) {
            terminarJuego(hcm.numero, JuegoMinas.FIN);
        }
    }
    public void marcarBandera(int f, int c, int numero, int op) {
        if(numero == hcm.numero) {
            if(op == JuegoMinas.MARCAR) {
                MTB[f][c].setIcon(new ImageIcon(getClass().getResource("/recursos/bandera.png")));
                MP[f][c] = numero;
                //
                hcm.marcas++;
                cantidadLibres--;
            } else {
                MTB[f][c].setIcon(null);
                MP[f][c] = JuegoMinas.LIBRE;
                //
                hcm.marcas--;
                cantidadLibres++;
            }
        } else {
            if(op == JuegoMinas.MARCAR) {
                MTB[f][c].setIcon(new ImageIcon(getClass().getResource("/recursos/bandera2.png")));
                MP[f][c] = numero;
                //
                hcm.marcasOponente++;
                cantidadLibres--;
            } else {
                MTB[f][c].setIcon(null);
                MP[f][c] = JuegoMinas.LIBRE;
                hcm.marcasOponente--;
                cantidadLibres++;
            }
        }
        
        //
        actualizarDatosMarcas();
        //
        
        if(cantidadLibres == 0) {
            terminarJuego(hcm.numero, JuegoMinas.FIN);
        }
    }
    public void setListOponentes(String[] lo) {
        if(lo.length == 1) {
            mostrarMensajeBarra("No hay openentes");
            return;
        }
        mostrarMensajeBarra("Lista de oponentes Recibida: " + new Date().toString());
        int i = 0;
        modeloLista.clear();
        while(i < lo.length) {
            if(lo[i].compareTo(hcm.nombre + "~" + hcm.id) != 0) {
                modeloLista.addElement(lo[i]);
            }
            i++;
        }
    }
    public void setMatrizJuego(String mj_str, String nombreOP) {
        //
        String[]mj = mj_str.split(",");
        MJ = new int[numeroFilas][numeroColumnas];
        MP = new int[numeroFilas][numeroColumnas];
        int k = 0;
        for(int i=0; i<numeroFilas; i++) {
            for(int j=0; j<numeroColumnas; j++) {
                MJ[i][j] = Integer.parseInt(mj[k]);
                MP[i][j] = 0;
                k++;
            }
        }
        textoNombreOP.setText(nombreOP);
    }//
    //
    public void mostrarMensajeBarra(String mensaje) {
        tiempoMensaje.stop();
        textoMensaje.setText("");
        tiempoMensaje.start();
        textoMensaje.setText(mensaje);
        
        System.out.println(mensaje);
    }
    public void mostrarMensaje(String mensaje) {
        JOptionPane.showMessageDialog(this, mensaje);
    }
    //
    public void iniciarJuego() {
        hBotonesJuego(true);
        hcm.marcas = 0;
        hcm.marcasOponente = 0;
        actualizarDatosMarcas();
        iniciado = true;
    }
    public void fueradeJuego() {
        hBotonesJuego(false);
        textoTu.setText("");
        textoOponente.setText("");
        textoNombreOP.setText("");
        iniciado = false;
    }
    public void actualizarDatosMarcas() {
        textoTu.setText(hcm.marcas + "/" + numeroMinas);
        textoOponente.setText(hcm.marcasOponente + "/" + numeroMinas);
    }
    //
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getActionCommand().compareTo("tiempo") == 0) {
            textoMensaje.setText("");
            tiempoMensaje.stop();
            return;
        }

        JToggleButton t = (JToggleButton)ae.getSource();
        
        if(hcm.turno == false) {
            t.setSelected(false);
            mostrarMensajeBarra("No es tu Turno");
            return;
        }
        
        String []fc= ae.getActionCommand().split("-");
        int f = Integer.parseInt(fc[0]), c = Integer.parseInt(fc[1]);
        
        if(MP[f][c] == JuegoMinas.OCUPADA) {
            t.setSelected(false);
            return;
        }
        
        String e = hcm.enviarJugada(f, c);
        t.setSelected(false);
        if(e.isEmpty()) {
           marcar(f, c, hcm.numero);
           hcm.turno = false;
        } else {
           mostrarMensajeBarra(e);
        }
    }
    @Override
    public void mousePressed(MouseEvent me) {
        if(iniciado == false) {
            return;
        }
        
        if(me.getButton() == MouseEvent.BUTTON3) {
            JToggleButton t = (JToggleButton)me.getSource();
            String []fc= t.getActionCommand().split("-");
            
            if(hcm.turno == false) {
                t.setSelected(false);
                mostrarMensajeBarra("No es tu Turno");
                return;
            }
            
            int f = Integer.parseInt(fc[0]), 
                c = Integer.parseInt(fc[1]);
            
            if(MP[f][c] == JuegoMinas.OCUPADA) {
                return;
            }
            
            int m;
            if(MP[f][c] == JuegoMinas.LIBRE) {
                m = JuegoMinas.MARCAR;
            } else if(MP[f][c] == hcm.numero) {
                m = JuegoMinas.DESMARCAR;
            } else {
                return;
            }
            
            String e = hcm.enviarMarcaBandera(f, c, m);
            if(e.isEmpty()) {
               marcarBandera(f, c, hcm.numero, m);
               hcm.turno = false;
               
            } else {
               mostrarMensajeBarra(e);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Otros eventos de raton">
    @Override
    public void mouseClicked(MouseEvent me) {
        
    }  
    @Override
    public void mouseReleased(MouseEvent me) {
    }
    @Override
    public void mouseEntered(MouseEvent me) {
    }
    @Override
    public void mouseExited(MouseEvent me) { 
    }
    //</editor-fold>
    
    // recuros
    private ImageIcon recursoIconoNumero(String nombre) {
        return new ImageIcon(
                getClass().getResource("/numeros/" + nombre + ".png"));
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAbandonar;
    private javax.swing.JButton botonConectar;
    private javax.swing.JButton botonDesconectar;
    private javax.swing.JButton botonSolicitud;
    private javax.swing.JToggleButton cuadroTB_00;
    private javax.swing.JToggleButton cuadroTB_01;
    private javax.swing.JToggleButton cuadroTB_02;
    private javax.swing.JToggleButton cuadroTB_03;
    private javax.swing.JToggleButton cuadroTB_04;
    private javax.swing.JToggleButton cuadroTB_10;
    private javax.swing.JToggleButton cuadroTB_11;
    private javax.swing.JToggleButton cuadroTB_12;
    private javax.swing.JToggleButton cuadroTB_13;
    private javax.swing.JToggleButton cuadroTB_14;
    private javax.swing.JToggleButton cuadroTB_20;
    private javax.swing.JToggleButton cudroTB_21;
    private javax.swing.JToggleButton cudroTB_22;
    private javax.swing.JToggleButton cudroTB_23;
    private javax.swing.JToggleButton cudroTB_24;
    private javax.swing.JToggleButton cudroTB_30;
    private javax.swing.JToggleButton cudroTB_31;
    private javax.swing.JToggleButton cudroTB_32;
    private javax.swing.JToggleButton cudroTB_33;
    private javax.swing.JToggleButton cudroTB_34;
    private javax.swing.JToggleButton cudroTB_40;
    private javax.swing.JToggleButton cudroTB_41;
    private javax.swing.JToggleButton cudroTB_42;
    private javax.swing.JToggleButton cudroTB_43;
    private javax.swing.JToggleButton cudroTB_44;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton10;
    private javax.swing.JToggleButton jToggleButton11;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JToggleButton jToggleButton3;
    private javax.swing.JToggleButton jToggleButton4;
    private javax.swing.JToggleButton jToggleButton5;
    private javax.swing.JToggleButton jToggleButton6;
    private javax.swing.JToggleButton jToggleButton7;
    private javax.swing.JToggleButton jToggleButton8;
    private javax.swing.JToggleButton jToggleButton9;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JList listOponentes;
    private javax.swing.JPanel panelJuego;
    private javax.swing.JTextField textoDirIP;
    private javax.swing.JLabel textoMensaje;
    private javax.swing.JTextField textoNombre;
    private javax.swing.JTextField textoNombreOP;
    private javax.swing.JTextField textoOponente;
    private javax.swing.JTextField textoPuerto;
    private javax.swing.JTextField textoTu;
    // End of variables declaration//GEN-END:variables
    
    private int [][]MJ;
    private int [][]MP;
    private int numeroFilas, numeroColumnas;
    private int cantidadLibres;
    private int numeroMinas;
    //
    private Socket socket;
    private HiloConexion hcm;
    
    //
    private JToggleButton [][]MTB;
    private ImageIcon[] iconosNumeros;
    private int PUERTO = 2013;
    //
    private Timer tiempoMensaje;
    public DefaultListModel <String> modeloLista;
    //
    public boolean iniciado;
}
