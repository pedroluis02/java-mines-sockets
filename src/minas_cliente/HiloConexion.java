/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package minas_cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JOptionPane;
import mensajes.Mensaje;

/**
 *
 * @author pedroluis
 */
public class HiloConexion extends Thread {
    private Socket socket;
    private DataInputStream flujoLectura;
    private DataOutputStream flujoEscritura;
    //
    public int idJuego;
    //
    public String id;
    public int numero;
    public String nombre;
    public boolean turno;
    public int marcas;
    //
    public String idOponente;
    public int marcasOponente;
    
    private ClienteGUI clienteGUI;

    public HiloConexion(Socket socket, String nombre, ClienteGUI clienteGUI) {
        this.socket = socket;
        this.clienteGUI = clienteGUI;
        this.nombre = nombre;
        
        try {
            this.flujoLectura = new DataInputStream(socket.getInputStream());
            this.flujoEscritura = new DataOutputStream(socket.getOutputStream());
        } catch(Exception ex) {
            clienteGUI.mostrarMensajeBarra(ex.getMessage());
        }
        id = "";
        numero = -1;
        idJuego = -1;
        idOponente = "";
        turno = false;
        //
        marcas = 0;
        marcasOponente = 0;
    }
    @Override
    public void run() {
        int opcion;
        try {
            while(true) {
                opcion = flujoLectura.readInt();
                switch(opcion) {
                    case Mensaje.REGISTRO: 
                        id = flujoLectura.readUTF();
                        clienteGUI.setTitle(clienteGUI.getTitle() + " - Id[" + id + "]");
                        clienteGUI.cambioGuiConectado();
                        // Envio de datos
                        clienteGUI.mostrarMensajeBarra("Enviando datos a servidor...");
                        enviarDatos();
                        break;
                    case Mensaje.OPONENTES:
                        String[] lo = flujoLectura.readUTF().split(",");
                        clienteGUI.setListOponentes(lo);
                        break;
                    case Mensaje.NUE_JUGADOR:
                        String op_nu = flujoLectura.readUTF();
                        String n_op = flujoLectura.readUTF();
                        clienteGUI.modeloLista.addElement(n_op + "~" + op_nu);
                        clienteGUI.mostrarMensajeBarra("Se conecto: [" + n_op + " - " + op_nu + "]");
                        break;
                    case Mensaje.DES_JUGADOR:
                        String idE = flujoLectura.readUTF();
                        String nE = flujoLectura.readUTF();
                        clienteGUI.modeloLista.removeElement(nE + "~" + idE);
                        clienteGUI.mostrarMensajeBarra("Se desconecto: " + nE + " - " + idE);
                        if(idOponente.compareTo(idE) == 0) {
                            clienteGUI.mostrarMensaje("Tu oponente se ha desconectado");
                            clienteGUI.fueradeJuego();
                        }
                        break;
                    case Mensaje.SOLICITUD:
                        String op_ = flujoLectura.readUTF(),
                               op_n = flujoLectura.readUTF();
                        int rpta = JOptionPane.showConfirmDialog(clienteGUI, 
                                    "Solicita juego: " + op_n  + " - " + op_,
                                    "\nConfirmación de Juego", JOptionPane.OK_CANCEL_OPTION);
                        // Rpta
                        flujoEscritura.writeInt(Mensaje.RPTA_SOLICITUD);
                        flujoEscritura.writeUTF(op_);
                        if(rpta == JOptionPane.OK_OPTION) {
                            flujoEscritura.writeInt(Mensaje.SI);
                        } else {
                            flujoEscritura.writeInt(Mensaje.NO);
                        }
                        break;
                    case Mensaje.RPTA_SOLICITUD:
                        String de = flujoLectura.readUTF(),
                               nde = flujoLectura.readUTF();
                        int res = flujoLectura.readInt();
                        if(res == Mensaje.SI) {
                            clienteGUI.mostrarMensajeBarra("Solicitud a "+ nde + " - " + de + ": aceptada");
                        } else if(res == Mensaje.NO) {
                            clienteGUI.mostrarMensajeBarra("Solicitud a "+ nde + " - " + de + ": rechazada");
                        } else if(res == Mensaje.OCUPADO) {
                            clienteGUI.mostrarMensajeBarra("Solicitud a "+ nde + " - " + de + ": esta en ocupado en juego");
                        }
                        break;
                    case Mensaje.UNIRSE:
                        clienteGUI.mostrarMensaje(flujoLectura.readUTF());
                        break;
                    case Mensaje.INICIO: 
                        idJuego = flujoLectura.readInt();
                        numero = flujoLectura.readInt();
                        // datos oponente
                        idOponente = flujoLectura.readUTF();
                        String nom = flujoLectura.readUTF();
                        // turno inicial en ele juego
                        turno = flujoLectura.readBoolean();
                        
                        // matriz de juego
                        String cm = flujoLectura.readUTF();
                        clienteGUI.setMatrizJuego(cm, nom);
                        clienteGUI.iniciarJuego();
                        break;
                    case Mensaje.JUGADA:  
                        int f = flujoLectura.readInt(),
                            c = flujoLectura.readInt(),
                            n_ = flujoLectura.readInt(); 
                        clienteGUI.marcar(f, c, n_);
                        turno = true;
                        break;
                    case Mensaje.BANDERA:  
                        int f1 = flujoLectura.readInt(),
                            c1 = flujoLectura.readInt(),
                            op = flujoLectura.readInt(),
                            n = flujoLectura.readInt(); 
                        clienteGUI.marcarBandera(f1, c1, n, op);
                        turno = true;
                        break;
                }
            }
        } catch(IOException ex)  {
            clienteGUI.mostrarMensajeBarra(ex.getMessage());
        }
        
        clienteGUI.cambioGuiDesconectado();
        
        if(idJuego != -1) {
            clienteGUI.mostrarMensaje("Conexión con servidor perdida: juego finalizado");
        } else {
            clienteGUI.mostrarMensajeBarra("Conexión con servidor: perdida");
        }
    }
    //
    public String enviarDatos() {
        try {
            flujoEscritura.writeInt(Mensaje.DATOS);
            flujoEscritura.writeUTF(nombre);
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    public String enviarSolicitud(String idO) {
        try {
            flujoEscritura.writeInt(Mensaje.SOLICITUD);
            flujoEscritura.writeUTF(idO);
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    public String enviarJugada(int fila, int columna) {
        try {
            flujoEscritura.writeInt(Mensaje.JUGADA);
            //
            flujoEscritura.writeInt(fila);
            flujoEscritura.writeInt(columna);
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    public String enviarMarcaBandera(int fila, int columna, int tipo) {
        try {
            flujoEscritura.writeInt(Mensaje.BANDERA);
            flujoEscritura.writeInt(fila);
            flujoEscritura.writeInt(columna);
            flujoEscritura.writeInt(tipo);
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
}
