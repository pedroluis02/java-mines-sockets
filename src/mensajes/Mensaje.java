/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mensajes;

/**
 *
 * @author pedroluis
 */
public class Mensaje {
    public static final int REGISTRO = 0;
    public static final int OPONENTES = 1;
    public static final int NUE_JUGADOR = 2;
    public static final int DES_JUGADOR = 3;
    public static final int SOLICITUD = 4;
    public static final int RPTA_SOLICITUD = 5;
    public static final int UNIRSE = 6;
    public static final int INICIO = 7;
    public static final int JUGADA = 8;
    public static final int BANDERA = 9;
    //
    public static final int DATOS = 10;
    //
    public static final int SI = 11;
    public static final int NO = 12;
    public static final int OCUPADO = 13;
}
